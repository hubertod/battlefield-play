﻿using BattlefieldPlay.BusinessLogic;
using BattlefieldPlay.Models;
using NUnit.Framework;
using Shouldly;
using System.Linq;

namespace BattlefieldPlay.NUnit.Tests
{
    [TestFixture]
    public class WarShipFactoryTests
    {
        private WarShipFactory _sut;

        [SetUp]
        public void SetUp()
        {
            _sut = new WarShipFactory();
        }

        [TestCase(1, 2, Direction.Horizontal, 10, 10, 2)]
        [TestCase(10, 5, Direction.Vertical, 5, 10, 9)]
        public void Create___Should_return_warship(int initialX, int initialY, Direction direction, int shipLength, int finalX, int finalY)
        {
            //Setup
            var initialCoordinate = new SquareCoordinate
            {
                X = initialX,
                Y = initialY
            };

            //Action
            var warShip = _sut.Create("A", "B", initialCoordinate, direction, shipLength);

            //Assert
            this.ShouldSatisfyAllConditions(
                () => warShip.OccupiedSquares.Count.ShouldBe(shipLength),
                () => warShip.OccupiedSquares.First().X.ShouldBe(initialX),
                () => warShip.OccupiedSquares.First().Y.ShouldBe(initialY),
                () => warShip.OccupiedSquares.Last().X.ShouldBe(finalX),
                () => warShip.OccupiedSquares.Last().Y.ShouldBe(finalY)
            );
        }
    }
}
