﻿using BattlefieldPlay.BusinessLogic;
using BattlefieldPlay.Models;
using NUnit.Framework;
using Shouldly;

namespace BattlefieldPlay.NUnit.Tests
{
    [TestFixture]
    public class BoardValidationTests
    {
        private BoardValidation _sut;

        [SetUp]
        public void SetUp()
        {
            _sut = new BoardValidation();
        }

        [TestCase(9, 9, Direction.Vertical, 3, true)]
        [TestCase(1, 2, Direction.Horizontal, 5, false)]
        [TestCase(1, 2, Direction.Horizontal, 10, true)]
        public void BoardValidation___Calculate_if_ship_is_outside_the_board(int initialX, int initialY, Direction direction, int shipLength, bool outcome)
        {
            //Setup
            var board = new BoardFactory().Create("Australia", 10);
            var warShip = new WarShipFactory().Create("Australia", "HMAS Adelaide", new SquareCoordinate { X= initialX, Y= initialY }, direction, shipLength);

            //Action
            var result = _sut.ShipShouldNotBeOutsideOfTheBoard(board, warShip);

            //Assert
            result.ShouldBe(outcome);
        }

        [Test]
        public void BoardValidation___When_overlaps___Then_return_true()
        {
            //Setup
            var shipA = new WarShipFactory().Create("Country A", "Ship A", new SquareCoordinate { X = 3, Y = 0 }, Direction.Vertical, 10);
            var shipB = new WarShipFactory().Create("Country A", "Ship B", new SquareCoordinate { X = 2, Y = 2}, Direction.Horizontal, 10);

            //Action
            var result = _sut.ShipShouldNotOverlap(shipA, shipB);

            //Assert
            result.ShouldBe(true);
        }

        [Test]
        public void BoardValidation___When_not_overlapping___Then_return_false()
        {
            //Setup
            var shipA = new WarShipFactory().Create("Country A", "Ship A", new SquareCoordinate { X = 3, Y = 3 }, Direction.Horizontal, 10);
            var shipB = new WarShipFactory().Create("Country A", "Ship B", new SquareCoordinate { X = 2, Y = 2 }, Direction.Horizontal, 10);

            //Action
            var result = _sut.ShipShouldNotOverlap(shipA, shipB);

            //Assert
            result.ShouldBe(false);
        }
    }
}
