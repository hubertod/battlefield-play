﻿using BattlefieldPlay.BusinessLogic;
using BattlefieldPlay.Interfaces;
using BattlefieldPlay.Models;
using NUnit.Framework;
using Shouldly;
using Moq;

namespace BattlefieldPlay.NUnit.Tests
{
    public class WarShipServiceTests
    {
        private WarShipService _sut;

        private Mock<IWarShipRepository> _warShipRepository;

        [SetUp]
        public void SetUp()
        {
            _warShipRepository = new Mock<IWarShipRepository>();

            _sut = new WarShipService(
                _warShipRepository.Object);
        }

        [TestCase(9, 9, Direction.Vertical, 3, true)]
        [TestCase(3, 10, Direction.Vertical, 3, false)]
        public void HitAnyOccupiedSq___if_match___return_true(int initialX, int initialY, Direction direction, int shipLength, bool outcome)
        {
            // Setup
            var warShip = new WarShipFactory().Create("Australia", "HMAS Adelaide", new SquareCoordinate { X= initialX, Y= initialY }, direction, shipLength);

            // Action
            var result = _sut.CheckIfHit(warShip, new SquareCoordinate {X = 9, Y = 10});

            // Assert
            result.ShouldBe(outcome);
        }

        [TestCase(9, 9, Direction.Vertical, 3, false)]
        public void HitAnyOccupiedSq___if_already_hit__return_false(int initialX, int initialY, Direction direction, int shipLength, bool outcome)
        {
            // Setup
            var warShip = new WarShipFactory().Create("Australia", "HMAS Adelaide", new SquareCoordinate { X= initialX, Y= initialY }, direction, shipLength);
            warShip.HitOccupiedSquares.Add(new SquareCoordinate {X = 9, Y = 10});

            // Action
            var result = _sut.CheckIfHit(warShip, new SquareCoordinate {X = 9, Y = 10});

            // Assert
            result.ShouldBe(outcome);
        }

        [TestCase(9, 9, Direction.Vertical, 3, false)]
        public void HitAnyOccupiedSq___if_not_active__return_false(int initialX, int initialY, Direction direction, int shipLength, bool outcome)
        {
            // Setup
            var warShip = new WarShipFactory().Create("Australia", "HMAS Adelaide", new SquareCoordinate { X= initialX, Y= initialY }, direction, shipLength);
            warShip.HitOccupiedSquares.Add(new SquareCoordinate {X = 9, Y = 9});
            warShip.HitOccupiedSquares.Add(new SquareCoordinate {X = 9, Y = 10});
            warShip.HitOccupiedSquares.Add(new SquareCoordinate {X = 9, Y = 11});

            // Action
            var result = _sut.CheckIfHit(warShip, new SquareCoordinate {X = 9, Y = 10});

            // Assert
            result.ShouldBe(outcome);
        }
    }
}
