﻿using BattlefieldPlay.BusinessLogic;
using BattlefieldPlay.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;

namespace BattlefieldPlay
{
    class Program
    {
        static void Main(string[] args)
        {
            //setup our DI
            var serviceProvider = new ServiceCollection()
                .AddLogging()
                .AddSingleton<IWarShipRepository, WarShipRepository>()
                .AddSingleton<IPlayerRepository, PlayerRepository>()
                .AddSingleton<IBoardRepository, BoardRepository>()
                .AddSingleton<IWarShipFactory, WarShipFactory>()
                .AddSingleton<IBoardFactory, BoardFactory>()
                .AddSingleton<IBoardValidation, BoardValidation>()
                .AddSingleton<IWarShipService, WarShipService>()
                .AddSingleton<IGamePlayService, GamePlayService>()
                .AddSingleton<IGameRunner, GameRunner>()
                .BuildServiceProvider();

            //configure console logging
            serviceProvider
                .GetService<ILoggerFactory>()
                .AddConsole(LogLevel.Information);

            var runner = serviceProvider.GetService<IGameRunner>();
            runner.InitiateSamples();
            runner.Play();

            Console.ReadLine();
        }
    }
}
