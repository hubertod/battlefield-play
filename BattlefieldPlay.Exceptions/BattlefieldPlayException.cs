﻿using BattlefieldPlay.Interfaces;
using BattlefieldPlay.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BattlefieldPlay.Exceptions
{
    public class BattlefieldPlayException : Exception, IApplicationException
    {
        public BattlefieldPlayException(
            BattlefieldPlayError errorCode,
            Exception ex = null,
            IList<string> erorMessageParameters = null) 
            : base($"BattlefieldPlayError.{errorCode} ({errorCode:d})", ex)
        {
            ErrorCode = errorCode;
            ErrorMessageParameters = erorMessageParameters;
        }

        private IList<string> _errorMessageParameters = new string[0];
        public IList<string> ErrorMessageParameters
        {
            get { return _errorMessageParameters; }
            set
            {
                if (value == null)
                {
                    _errorMessageParameters = new string[0];
                }
                else
                {
                    _errorMessageParameters = value;
                }
            }
        }
        
        public BattlefieldPlayError ErrorCode { get; }
    }
}
