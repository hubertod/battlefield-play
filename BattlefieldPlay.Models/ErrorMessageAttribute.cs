﻿using System;

namespace BattlefieldPlay.Models
{
    public class ErrorMessageAttribute : Attribute
    {
        public string Message { get; }

        public ErrorMessageAttribute(string message)
        {
            Message = message;
        }
    }
}
