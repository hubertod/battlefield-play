﻿namespace BattlefieldPlay.Models
{
    public enum BattlefieldPlayError
    {
        [ErrorMessage("Ship's location is outside the board, please set a different location.")]
        ShipShouldNotBeOutsideOfTheBoardError = 10001,

        [ErrorMessage("Ship should not overlap to other ship, please set a different location.")]
        ShipShouldNotOverlapError = 10002
    }
}
