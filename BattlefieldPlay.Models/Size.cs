﻿namespace BattlefieldPlay.Models
{
    public class Size
    {
        public int HorizontalSquareCount;
        public int VerticalSquareCount;
    }
}
