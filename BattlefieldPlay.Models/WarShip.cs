﻿using System.Collections.Generic;
using System.Linq;

namespace BattlefieldPlay.Models
{

    public class WarShip
    {
        public BattlefieldPlayer BattlefieldPlayer { get; set; }

        public string ShipName { get; set; }

        public IList<SquareCoordinate> OccupiedSquares; // Ship's coordinate on the board
        public IList<SquareCoordinate> HitOccupiedSquares; // where the battleship has been shot

        // Check if the ship still active (not yet sunk)
        public bool IsActive()
        {
            return OccupiedSquares.Count > HitOccupiedSquares.Count;
        }

        // Check if ship is already sunk
        public bool HasSunk()
        {
            return OccupiedSquares.Count == HitOccupiedSquares.Count;
        }
    }
}
