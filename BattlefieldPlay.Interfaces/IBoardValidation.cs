﻿using System.Collections.Generic;
using BattlefieldPlay.Models;

namespace BattlefieldPlay.Interfaces
{
    public interface IBoardValidation
    {
        bool ShipShouldNotOverlap(IList<WarShip> existingWarShips, WarShip newWarShip, bool throwException = false);
        bool ShipShouldNotOverlap(WarShip shipA, WarShip shipB, bool throwException = false);
        bool ShipShouldNotBeOutsideOfTheBoard(Board board, WarShip warShip, bool throwException = false);
    }
}