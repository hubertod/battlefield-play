﻿using BattlefieldPlay.Models;

namespace BattlefieldPlay.Interfaces
{
    public interface IWarShipFactory
    {
        WarShip Create(string country, string shipName, SquareCoordinate initialCoordinate, Direction direction, int lengthOfShip);
    }
}