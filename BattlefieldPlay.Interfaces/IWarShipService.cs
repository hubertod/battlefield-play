﻿using System.Collections.Generic;
using BattlefieldPlay.Models;

namespace BattlefieldPlay.Interfaces
{
    public interface IWarShipService
    {
        void Add(WarShip warShip);
        IList<WarShip> Get(string playerName);
        IList<BattlefieldPlayer> GetActivePlayers();
        bool HasEnoughActivePlayers();
        bool CheckIfHit(WarShip warShip, SquareCoordinate shootCoordinate);
    }
}