﻿using System.Collections.Generic;
using BattlefieldPlay.Models;

namespace BattlefieldPlay.Interfaces
{
    public interface IPlayerRepository
    {
        void Add(string playerName);
        IList<BattlefieldPlayer> GetAll();
    }
}