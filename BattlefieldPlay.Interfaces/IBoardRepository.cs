﻿using BattlefieldPlay.Models;

namespace BattlefieldPlay.Interfaces
{
    public interface IBoardRepository
    {
        void Add(string countryName, int boardSize);
        Board Get(string playerName);
    }
}