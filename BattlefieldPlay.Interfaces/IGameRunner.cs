﻿namespace BattlefieldPlay.Interfaces
{
    public interface IGameRunner
    {
        void InitiateSamples();
        void Play();
    }
}