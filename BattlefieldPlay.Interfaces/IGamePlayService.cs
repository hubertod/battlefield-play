﻿using BattlefieldPlay.Models;
using System.Collections.Generic;

namespace BattlefieldPlay.Interfaces
{
    public interface IGamePlayService
    {
        void SetupBoard(string playerName, int boardSize);
        bool Shoot(string targetPlayer, SquareCoordinate shootCoordinate);
        void PlaceWarShip(WarShip warShip);
        IList<BattlefieldPlayer> GetPlayers();
        IList<BattlefieldPlayer> GetPlayersWithActiveShips();
        bool HasEnoughActivePlayers();
    }
}