﻿using BattlefieldPlay.Models;
using System.Collections.Generic;

namespace BattlefieldPlay.Interfaces
{
    public interface IWarShipRepository
    {
        void Add(WarShip warShip);
        void AddHit(WarShip warShip, SquareCoordinate shootCoordinate);
        IList<WarShip> Get(string playerName);
        IList<WarShip> GetAll();
    }
}