﻿using BattlefieldPlay.Models;
using System.Collections.Generic;

namespace BattlefieldPlay.Interfaces
{
    public interface IApplicationException
    {
        BattlefieldPlayError ErrorCode { get; }
        IList<string> ErrorMessageParameters { get; }
    }
}
