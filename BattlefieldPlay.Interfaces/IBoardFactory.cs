﻿using BattlefieldPlay.Models;

namespace BattlefieldPlay.Interfaces
{
    public interface IBoardFactory
    {
        Board Create(string countryName, int boardSize);
    }
}