# Battlefield Play

This solution was prepared as part of the home excecise required by FlareHR

## Software Requirements

The project was developed using:

1. Visual Studio 2017 Community Edition
2. Net Framework 4.6.1
3. nuget

To view the project, please make sure the computer has all of the software installed.

## Solutions

The solution contains 6 different projects:

1. **BattlefieldPlay** 

      - This is the console application
   
2. **BattlefieldPlay.BusinessLogic**

      - All business logic to run the game. 
      - This project also contains GameRunner.cs, the runner that will setup boards, deploy warships, and simulate shootings.
      - To keep the demonstration simple, all ships deployment and shootings are hardcoded
      - GameRunner.cs can be extended to accept input from the user through the console.
   
3. **BattlefieldPlay.Exceptions**

      - Custom exceptions just to make it easier to assign error number and user-friendly message

4. **BattlefieldPlay.Interfaces**

      - As in the name, the project contains all the interfaces for business logic

5. **BattlefieldPlay.Models**

      - As in the name, the project contains the models that I'm using

6. **BattlefieldPlay.NUnit.Tests**

      - Some unit tests to test the logic
   
   
## Basic Instructions

To run the application, please do the followings:

1. Open the solution in Visual Studio
2. Restore all the nuget packages
3. Make sure to set the console project as the startup application
4. Run the solution to execute the simulation
5. The game simulation result (e.g. hit, miss and winner info) will be displayed in the console command line