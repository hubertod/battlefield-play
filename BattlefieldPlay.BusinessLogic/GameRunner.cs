﻿using BattlefieldPlay.Interfaces;
using BattlefieldPlay.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BattlefieldPlay.BusinessLogic
{
    public class GameRunner : IGameRunner
    {
        private readonly IGamePlayService _gamePlayService;
        private readonly IWarShipFactory _warShipFactory;
        private readonly ILogger<GameRunner> _logger;

        public GameRunner(
            IGamePlayService gamePlayService,
            IWarShipFactory warShipFactory,
            ILoggerFactory loggerFactory)
        {
            _gamePlayService = gamePlayService;
            _warShipFactory = warShipFactory;
            _logger = loggerFactory.CreateLogger<GameRunner>();
        }

        public void InitiateSamples()
        {
            SetupSamplePlayerBoards();
            SetupSampleWarShips();
        }

        private void SetupSamplePlayerBoards()
        {
            _gamePlayService.SetupBoard("USA", 10);
            _gamePlayService.SetupBoard("French", 10);
        }

        private void SetupSampleWarShips()
        {
            var frenchWarShipA = _warShipFactory.Create("French", "Amiral Duperre", new SquareCoordinate { X = 1, Y = 1 }, Direction.Horizontal, 5);
            _gamePlayService.PlaceWarShip(frenchWarShipA);

            var frenchWarShipB = _warShipFactory.Create("French", "Charles Martel", new SquareCoordinate { X = 3, Y = 3 }, Direction.Horizontal, 2);
            _gamePlayService.PlaceWarShip(frenchWarShipB);

            var usaWarShipA = _warShipFactory.Create("USA", "USS Alaska", new SquareCoordinate { X = 2, Y = 4 }, Direction.Horizontal, 5);
            _gamePlayService.PlaceWarShip(usaWarShipA);

            var usaWarShipB = _warShipFactory.Create("USA", "USS Colorado", new SquareCoordinate { X = 6, Y = 7 }, Direction.Vertical, 2);
            _gamePlayService.PlaceWarShip(usaWarShipB);
        }

        public void Play()
        {
            List<Action> shoots = SetupSampleShootActions();

            foreach (var shootTheShip in shoots)
            {
                shootTheShip();

                if (!_gamePlayService.HasEnoughActivePlayers())
                {
                    DisplayWinner();
                    break;
                }
            }
        }

        private void DisplayWinner()
        {
            var remainingPlayers = _gamePlayService.GetPlayersWithActiveShips();
            var winner = remainingPlayers.First();
            _logger.LogInformation($"{winner.PlayerName} has won the game");
        }

        private List<Action> SetupSampleShootActions()
        {
            return new List<Action>
            {
                delegate { _gamePlayService.Shoot("French", new SquareCoordinate { X = 2, Y = 1 }); },
                delegate { _gamePlayService.Shoot("USA", new SquareCoordinate { X = 6, Y = 7 }); },
                delegate { _gamePlayService.Shoot("French", new SquareCoordinate { X = 1, Y = 2 }); },
                delegate { _gamePlayService.Shoot("USA", new SquareCoordinate { X = 6, Y = 8 }); },
                delegate { _gamePlayService.Shoot("French", new SquareCoordinate { X = 5, Y = 3 }); },
                delegate {  _gamePlayService.Shoot("USA", new SquareCoordinate { X = 5, Y = 1 }); },
                delegate { _gamePlayService.Shoot("French", new SquareCoordinate { X = 10, Y = 10 }); },

                // For to finish the demo game
                delegate { _gamePlayService.Shoot("USA", new SquareCoordinate { X = 2, Y = 4 }); },
                delegate { _gamePlayService.Shoot("USA", new SquareCoordinate { X = 3, Y = 4 }); },
                delegate { _gamePlayService.Shoot("USA", new SquareCoordinate { X = 4, Y = 4 }); },
                delegate { _gamePlayService.Shoot("USA", new SquareCoordinate { X = 5, Y = 4 }); },
                delegate { _gamePlayService.Shoot("USA", new SquareCoordinate { X = 6, Y = 4 }); }
            };
        }
    }
}
