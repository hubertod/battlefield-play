﻿using BattlefieldPlay.Interfaces;
using BattlefieldPlay.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BattlefieldPlay.BusinessLogic
{
    public class WarShipRepository : IWarShipRepository
    {
        private static IList<WarShip> _warShipRepository;

        public WarShipRepository()
        {
            _warShipRepository = new List<WarShip>();
        }

        public void Add(WarShip warShip)
        {
            _warShipRepository.Add(warShip);
        }

        public IList<WarShip> Get(string playerName)
        {
            return _warShipRepository.Where(w => w.BattlefieldPlayer.PlayerName == playerName)
                .ToList();
        }

        public IList<WarShip> GetAll()
        {
            return _warShipRepository.ToList();
        }

        public void AddHit(WarShip warShip, SquareCoordinate shootCoordinate)
        {
            warShip.HitOccupiedSquares.Add(shootCoordinate);
        }
    }

}
