﻿using System;
using BattlefieldPlay.Interfaces;
using BattlefieldPlay.Models;
using System.Collections.Generic;
using System.Linq;

namespace BattlefieldPlay.BusinessLogic
{
    public class WarShipService : IWarShipService
    {
        private readonly IWarShipRepository _warShipRepository;

        public WarShipService(
            IWarShipRepository warShipRepository)
        {
            _warShipRepository = warShipRepository;
        }

        public void Add(WarShip warShip)
        {
            _warShipRepository.Add(warShip);
        }

        public IList<WarShip> Get(string playerName)
        {
            return _warShipRepository.Get(playerName)
                .ToList();
        }

        public IList<BattlefieldPlayer> GetActivePlayers()
        {
            var warShips = _warShipRepository.GetAll();

            var playerWarShips =
                warShips.GroupBy(info => info.BattlefieldPlayer.PlayerName)
                    .Select(group => new
                    {
                        PlayerName = group.Key,
                        TotalWarShips = group.Count(),
                        TotalActive = warShips.Count(
                            x => x.BattlefieldPlayer.PlayerName.Equals(group.Key, StringComparison.OrdinalIgnoreCase) &&
                                 x.IsActive())
                    })
                    .OrderBy(x => x.PlayerName);

            var activePlayers = playerWarShips.Where(cw => cw.TotalActive > 0)
                .Select(c => c.PlayerName)
                .ToList();

            return activePlayers
                .Select(player => new BattlefieldPlayer {PlayerName = player})
                .ToList();
        }

        public bool HasEnoughActivePlayers()
        {
            var activePlayers = GetActivePlayers();
            return activePlayers.Count > 1;
        }

        // Am I being hit?
        public bool CheckIfHit(WarShip warShip, SquareCoordinate shootCoordinate)
        {
            var hit = false;

            if (warShip.IsActive()) // if not yet sunk
            {
                // hit any occupied square?
                var hitAnyOccupiedSq = HitAnyOccupiedSq(warShip, shootCoordinate); // is that shoot hit any occupied sq?
                var wasHitBefore = HasThatSqBeenHitBefore(warShip, shootCoordinate); // has that square been hit before?

                if (hitAnyOccupiedSq && !wasHitBefore) 
                {
                    hit = true;
                    _warShipRepository.AddHit(warShip, shootCoordinate);
                }
            }

            return hit;
        }

        public bool HasThatSqBeenHitBefore(WarShip warShip, SquareCoordinate shootCoordinate)
        {
            return warShip.HitOccupiedSquares.Any(alreadyHitOccupiedSq =>
                alreadyHitOccupiedSq.X == shootCoordinate.X &&
                alreadyHitOccupiedSq.Y == shootCoordinate.Y);
        }

        public bool HitAnyOccupiedSq(WarShip warShip, SquareCoordinate shootCoordinate)
        {
            return warShip.OccupiedSquares.Any(occupiedSq =>
                occupiedSq.X == shootCoordinate.X &&
                occupiedSq.Y == shootCoordinate.Y);
        }
    }

}
