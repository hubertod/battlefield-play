﻿using BattlefieldPlay.Interfaces;
using BattlefieldPlay.Models;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace BattlefieldPlay.BusinessLogic
{

    public class GamePlayService : IGamePlayService
    {
        private readonly IPlayerRepository _playerRepository;
        private readonly IWarShipService _warShipService;
        private readonly IBoardRepository _boardRepository;
        private readonly IBoardFactory _boardFactory;
        private readonly IBoardValidation _boardValidation;
        private readonly ILogger<GamePlayService> _logger;

        public GamePlayService(
            IWarShipService warShipService,
            IPlayerRepository playerRepository,
            IBoardRepository boardRepository,
            IBoardFactory boardFactory,
            ILoggerFactory loggerFactory,
            IBoardValidation boardValidation)
        {
            _warShipService = warShipService;
            _playerRepository = playerRepository;
            _boardRepository = boardRepository;
            _boardFactory = boardFactory;
            _boardValidation = boardValidation;

            _logger = loggerFactory.CreateLogger<GamePlayService>();
        }

        public void SetupBoard(string playerName, int boardSize)
        {
            _playerRepository.Add(playerName);
            _boardRepository.Add(playerName, boardSize);
        }

        public bool Shoot(string targetPlayer, SquareCoordinate shootingCoordinate)
        {
            var opponentWarShips = _warShipService.Get(targetPlayer);
            var hit = CheckHit(opponentWarShips, shootingCoordinate);

            if (!hit)
            {
                _logger.LogInformation("Miss...");
            }

            return hit;
        }

        private bool CheckHit(IList<WarShip> opponentWarShips, SquareCoordinate shootingCoordinate)
        {
            var hit = false;

            foreach (var targetWarShip in opponentWarShips)
            {
                hit = _warShipService.CheckIfHit(targetWarShip, shootingCoordinate);

                if (hit)
                {
                    _logger.LogInformation($"{targetWarShip.BattlefieldPlayer.PlayerName}'s WarShip '{targetWarShip.ShipName}' was hit!!!!");
                    break;
                }
            }

            return hit;
        }

        public void PlaceWarShip(WarShip warShip)
        {
            var board = _boardRepository.Get(warShip.BattlefieldPlayer.PlayerName);
            var warShips = _warShipService.Get(warShip.BattlefieldPlayer.PlayerName);

            _boardValidation.ShipShouldNotBeOutsideOfTheBoard(board, warShip, true);
            _boardValidation.ShipShouldNotOverlap(warShips, warShip, true);

            _warShipService.Add(warShip);
        }

        public IList<BattlefieldPlayer> GetPlayers()
        {
            return _playerRepository.GetAll();
        }

        public IList<BattlefieldPlayer> GetPlayersWithActiveShips()
        {
            return _warShipService.GetActivePlayers();
        }

        public bool HasEnoughActivePlayers()
        {
            return _warShipService.HasEnoughActivePlayers();
        }
    }

}
