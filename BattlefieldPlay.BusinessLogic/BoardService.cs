﻿using BattlefieldPlay.Interfaces;
using BattlefieldPlay.Models;

namespace BattlefieldPlay.BusinessLogic
{
    public class BoardFactory : IBoardFactory
    {
        public Board Create(string playerName, int boardSize)
        {
            return new Board
            {
                PlayerName = playerName,
                Size = new Size
                {
                    HorizontalSquareCount = boardSize,
                    VerticalSquareCount = boardSize
                }
            };
        }
    }
}
