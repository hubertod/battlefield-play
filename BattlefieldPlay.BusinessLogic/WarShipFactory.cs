﻿using BattlefieldPlay.Interfaces;
using BattlefieldPlay.Models;
using System.Collections.Generic;

namespace BattlefieldPlay.BusinessLogic
{
    public class WarShipFactory : IWarShipFactory
    {
        public WarShip Create(string playerName, string shipName, SquareCoordinate initialCoordinate, Direction direction, int lengthOfShip)
        {
            // Create the war ship
            var coordinateLocations = new List<SquareCoordinate>();
            SetOccupiedSquaresVerticalDirection(initialCoordinate, direction, lengthOfShip, coordinateLocations);
            SetOccupiedSquaresHorizontalDirection(initialCoordinate, direction, lengthOfShip, coordinateLocations);
            return ConstructShip(playerName, shipName, coordinateLocations);
        }

        private static WarShip ConstructShip(string playerName, string shipName, List<SquareCoordinate> coordinateLocations)
        {
            return new WarShip
            {
                BattlefieldPlayer = new BattlefieldPlayer { PlayerName = playerName },
                ShipName = shipName,
                OccupiedSquares = coordinateLocations,
                HitOccupiedSquares = new List<SquareCoordinate>()
            };
        }

        private static void SetOccupiedSquaresHorizontalDirection(SquareCoordinate initialCoordinate, Direction direction, int lengthOfShip, List<SquareCoordinate> coordinateLocations)
        {
            if (direction == Direction.Horizontal)
            {
                for (var idx = initialCoordinate.X; idx < initialCoordinate.X + lengthOfShip; idx++)
                {
                    coordinateLocations.Add(
                        new SquareCoordinate
                        {
                            Y = initialCoordinate.Y,
                            X = idx
                        }
                    );
                }
            }
        }

        private static void SetOccupiedSquaresVerticalDirection(SquareCoordinate initialCoordinate, Direction direction, int lengthOfShip, List<SquareCoordinate> coordinateLocations)
        {
            if (direction == Direction.Vertical)
            {
                for (var idy = initialCoordinate.Y; idy < initialCoordinate.Y + lengthOfShip; idy++)
                {
                    coordinateLocations.Add(
                        new SquareCoordinate
                        {
                            Y = idy,
                            X = initialCoordinate.X
                        }
                    );
                }
            }
        }
    }
}
