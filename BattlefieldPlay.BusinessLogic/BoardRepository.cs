﻿using BattlefieldPlay.Interfaces;
using BattlefieldPlay.Models;
using System.Collections.Generic;
using System.Linq;

namespace BattlefieldPlay.BusinessLogic
{
    public class BoardRepository : IBoardRepository
    {
        private readonly IBoardFactory _boardFactory;
        private static IList<Board> _boardRepository;

        public BoardRepository(
            IBoardFactory boardFactory)
        {
            _boardFactory = boardFactory;
            _boardRepository = new List<Board>();
        }

        public void Add(string playerName, int boardSize)
        {
            var existingBoard = _boardRepository.SingleOrDefault(b => b.PlayerName == playerName);

            if (existingBoard == null)
            { 
                // it's a new one, so add a board
                var board = _boardFactory.Create(playerName, boardSize);
                _boardRepository.Add(board);
            }
        }

        public Board Get(string playerName)
        {
            return _boardRepository.Single(b => b.PlayerName == playerName);
        }
    }
}
