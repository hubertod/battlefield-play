﻿using BattlefieldPlay.Exceptions;
using BattlefieldPlay.Interfaces;
using BattlefieldPlay.Models;
using System.Collections.Generic;

namespace BattlefieldPlay.BusinessLogic
{
    public class BoardValidation : IBoardValidation
    {
        public bool ShipShouldNotOverlap(IList<WarShip> existingWarShips, WarShip newWarShip, bool throwException = false)
        {
            var isOverlap = false;

            foreach (var existingWarShip in existingWarShips)
            {
                var overlap = ShipShouldNotOverlap(existingWarShip, newWarShip, throwException);
                if (overlap)
                {
                    isOverlap = true;
                    break;
                }
            }

            return isOverlap;
        }

        public bool ShipShouldNotOverlap(WarShip shipA, WarShip shipB, bool throwException = false)
        {
            var isOverlap = false;

            foreach (var shipACoordinate in shipA.OccupiedSquares)
            {
                foreach (var shipBCoordinate in shipB.OccupiedSquares)
                {
                    if (shipACoordinate.X == shipBCoordinate.X &&
                        shipACoordinate.Y == shipBCoordinate.Y)
                    {
                        isOverlap = true;
                        break;
                    }
                }
            }

            if (isOverlap && throwException)
            {
                throw new BattlefieldPlayException(BattlefieldPlayError.ShipShouldNotOverlapError);
            }

            return isOverlap;
        }

        public bool ShipShouldNotBeOutsideOfTheBoard(Board board, WarShip warShip, bool throwException = false)
        {
            var isOverlap = false;

            foreach (var loc in warShip.OccupiedSquares)
            {
                if (loc.X > board.Size.HorizontalSquareCount)
                {
                    isOverlap = true;
                    break;
                }

                if (loc.Y > board.Size.VerticalSquareCount)
                {
                    isOverlap = true;
                    break;
                }
            }

            if (isOverlap && throwException)
            {
                throw new BattlefieldPlayException(BattlefieldPlayError.ShipShouldNotBeOutsideOfTheBoardError);
            }

            return isOverlap;
        }
    }

}
