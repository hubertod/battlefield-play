﻿using BattlefieldPlay.Interfaces;
using System.Collections.Generic;
using System.Linq;
using BattlefieldPlay.Models;

namespace BattlefieldPlay.BusinessLogic
{
    public class PlayerRepository : IPlayerRepository
    {
        private static IList<string> _playerRepository;

        public PlayerRepository()
        {
            _playerRepository = new List<string>();
        }

        public void Add(string playerName)
        {
            _playerRepository.Add(playerName);
        }

        public IList<BattlefieldPlayer> GetAll()
        {
            return _playerRepository
                .Select(player => new BattlefieldPlayer {PlayerName = player})
                .ToList();
        }
    }

}
